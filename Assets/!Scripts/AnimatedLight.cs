using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedLight : MonoBehaviour
{

    public GameObject light1;
    public GameObject light2;
    public float timer;
    float interTimer;
    public bool l1On;
    public bool l2On;

    private void Start()
    {
        interTimer = timer;

        l1On = true;
        l2On = false;
    }

    private void Update()
    {
        if(l1On == true)
        {
            interTimer -= Time.deltaTime;

            if(interTimer <= 0)
            {
                l1On = false;
                light2.gameObject.SetActive(false);
                light1.gameObject.SetActive(true);
                l2On = true;
                interTimer = timer;
            }
        }
        else if(l2On == true)
        {
            interTimer -= Time.deltaTime;

            if (interTimer <= 0)
            {
                l2On = false;
                light1.gameObject.SetActive(false);
                light2.gameObject.SetActive(true);
                l1On = true;
                interTimer = timer;
            }
            
        }
    }
}
