using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour
{

    public Animator animator;
    public GameObject manager;
    public bool rightDoor;
    public bool leftDoor;

    private void Start()
    {

        //Cursor.lockState = CursorLockMode.Confined;
        //Cursor.visible = false;
    }

    private void Update()
    {
        Door();
    }

    void Door()
    {
        if (manager.gameObject.GetComponent<ManagerAnimator>().turnLeft == true)
        {
            if(rightDoor == true)
            {
                animator.SetBool("Open", false);
                animator.SetBool("Close", true);
            }
            else if(leftDoor == true)
            {
                animator.SetBool("Close", false);
                animator.SetBool("Open", true);
            }
        }
        else
        {
            if (manager.gameObject.GetComponent<ManagerAnimator>().turnRight == true)
            {
                if (rightDoor == true)
                {
                    animator.SetBool("Close", false);
                    animator.SetBool("Open", true);
                }
                else if (leftDoor == true)
                {
                    animator.SetBool("Open", false);
                    animator.SetBool("Close", true);
                }
            }

            else if(manager.gameObject.GetComponent<ManagerAnimator>().turnNormal == true)
            {
                animator.SetBool("Open", false);
                animator.SetBool("Close", false);
            }
        }
    }

}
