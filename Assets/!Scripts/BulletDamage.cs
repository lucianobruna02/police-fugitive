using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    public int bulletDamage = 5;


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Motor"))
        {
            Debug.LogError("Pego Motor");
            collision.gameObject.GetComponent<PoliceAI>().health -= bulletDamage;

        }
        else
        {

            if (collision.gameObject.CompareTag("Parabrisa"))
            {
                Debug.LogError("Pego Parabrisa");
            }
            else
            {
                if (collision.gameObject.CompareTag("Conductor"))
                {
                    Debug.LogError("Pego Conductor");
                    collision.gameObject.GetComponent<PoliceAI>().health -= collision.gameObject.GetComponent<PoliceAI>().health;
                    //collision.gameObject.GetComponent<PoliceAI>().maxVel = 0f;
                    //collision.gameObject.GetComponent<PoliceAI>().minVel = 0f;
                }
                //else
                //{
                //    if (this.gameObject.CompareTag("Rueda") && hayRueda == true)
                //    {
                //        hayRueda = false;
                //        Debug.LogError("Pego Rueda");
                //        AI.gameObject.GetComponent<PoliceAI>().maxVel -= 0.5f;
                //        AI.gameObject.GetComponent<PoliceAI>().minVel -= 0.5f;
                //    }
                //}
            }
        }
    }
}
