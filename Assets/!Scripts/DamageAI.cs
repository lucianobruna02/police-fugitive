using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageAI : MonoBehaviour
{

    //public bool hayRueda;
    public int bulletDamage = 5;

    public GameObject AI;
    public GameObject player;

    private void Awake()
    {
        player = GameObject.Find("Singleplayer");
    }

    void Start()
    {

        //if (this.CompareTag("Rueda"))
        //{
        //    hayRueda = true;
        //}
        //else
        //{
        //    hayRueda = false;
        //}
    }

    void Update()
    {
        if(AI.gameObject.GetComponent<PoliceAI>().health <= 0)
        {
            AI.gameObject.GetComponent<PoliceAI>().StopCar = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.LogError(this.gameObject.name);

        if (collision.gameObject.CompareTag("Bullet"))
        {
            //Debug.LogError(this.gameObject.name);
            if (this.gameObject.CompareTag("Motor") && RayCast.instance.motor == true)
            {
                Debug.LogError("Pego Motor");
                AI.gameObject.GetComponent<PoliceAI>().health -= bulletDamage;
            }
            else
            {
                if (this.gameObject.CompareTag("Parabrisa") && RayCast.instance.parabrisa == true)
                {
                    Debug.LogError("Pego Parabrisa");
                }
                else
                {
                    if (this.gameObject.CompareTag("Conductor") && RayCast.instance.conductor == true)
                    {
                        Debug.LogError("Pego Conductor");
                        AI.gameObject.GetComponent<PoliceAI>().health -= AI.gameObject.GetComponent<PoliceAI>().health;
                        AI.gameObject.GetComponent<PoliceAI>().maxVel = 0f;
                        AI.gameObject.GetComponent<PoliceAI>().minVel = 0f;
                    }
                    //else
                    //{
                    //    if (this.gameObject.CompareTag("Rueda") && hayRueda == true)
                    //    {
                    //        hayRueda = false;
                    //        Debug.LogError("Pego Rueda");
                    //        AI.gameObject.GetComponent<PoliceAI>().maxVel -= 0.5f;
                    //        AI.gameObject.GetComponent<PoliceAI>().minVel -= 0.5f;
                    //    }
                    //}
                }
            }
        }
    }
}
