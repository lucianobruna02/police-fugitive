using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTrigger : MonoBehaviour
{
    public GameObject enemies;
    public int count;
    int interCount;
    public GameObject spawner;
    public bool activate;
    //public bool canInstantiate;

    void Start()
    {
        activate = false;
        interCount = 0;
        //canInstantiate = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(activate == true)
        {
            //for (int i = 0; i < count; i++)
            //{
                
            //    //enemies[i].gameObject.SetActive(true);
            //    if (i >= count)
            //    {
            //        //Destroy(this.gameObject);
            //        this.gameObject.SetActive(false);
            //        activate = false;
            //    }
            //    else
            //    {
            //        Instantiate(enemies, spawner.transform.position, Quaternion.identity);
            //    }
            //}

            if(interCount < count)
            {
                Instantiate(enemies, spawner.transform.position, Quaternion.identity);
                interCount++;
            }
            else
            {
                this.gameObject.SetActive(false);
                activate = false;
            }

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerTrigger"))
        {
            activate = true;
        }
    }
}
