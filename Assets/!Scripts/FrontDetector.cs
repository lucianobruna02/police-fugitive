using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontDetector : MonoBehaviour
{
    public GameObject father;
    public bool enemyInRange;

    private void Start()
    {
        enemyInRange = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            enemyInRange = true;
            father.gameObject.GetComponent<PoliceAI>().maxVel -= 2;
            father.gameObject.GetComponent<PoliceAI>().normalVel -= 2;
            father.gameObject.GetComponent<PoliceAI>().minVel -= 2;
        }
        else
        {
            if (father.gameObject.GetComponent<PoliceAI>().maxVel < 41) father.gameObject.GetComponent<PoliceAI>().maxVel += 2;
            else if (father.gameObject.GetComponent<PoliceAI>().maxVel >= 41) father.gameObject.GetComponent<PoliceAI>().maxVel = 41;

            if (father.gameObject.GetComponent<PoliceAI>().normalVel < 39) father.gameObject.GetComponent<PoliceAI>().normalVel += 2;
            else if (father.gameObject.GetComponent<PoliceAI>().normalVel >= 39) father.gameObject.GetComponent<PoliceAI>().normalVel = 39;

            if (father.gameObject.GetComponent<PoliceAI>().minVel < 37) father.gameObject.GetComponent<PoliceAI>().minVel += 2;
            else if (father.gameObject.GetComponent<PoliceAI>().minVel >= 37) father.gameObject.GetComponent<PoliceAI>().minVel = 37;

            enemyInRange = false;
        }
    }
}
