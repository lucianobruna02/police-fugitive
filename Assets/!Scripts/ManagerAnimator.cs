using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerAnimator : MonoBehaviour
{

    public bool turnLeft;
    public bool turnRight;
    public bool turnNormal;

    private void Start()
    {
        turnLeft = false;
        turnRight = false;
        turnNormal = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("TurnLeft"))
        {
            turnNormal = false;
            turnLeft = true;
        }
        else
        {
            if (other.gameObject.CompareTag("TurnRight"))
            {
                turnNormal = false;
                turnRight = true;
            }
            else if (other.gameObject.CompareTag("Normal"))
            {
                turnRight = false;
                turnLeft = false;
                turnNormal = true;
            }
        }
    }
}
