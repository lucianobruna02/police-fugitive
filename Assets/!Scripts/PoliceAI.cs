using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class PoliceAI : MonoBehaviour
{
    public Transform target;

    public float closestRange, furthestRange, distance;

    //States
    public bool playerFar, playerNear, playerInArea, StopCar, destroyCar;

    public GameObject detectorFront;

    public int health = 100;

    public float maxVel/* = 41*/;
    public float normalVel/* = 39*/;
    public float minVel/* = 37*/;
    public float timer;
    float interTimer;

    private void Start()
    {
        normalVel = Random.Range(35, 40);
        this.gameObject.GetComponent<RichAI>().slowdownTime = Random.Range(5, 10);

        maxVel = normalVel + 2;
        minVel = normalVel - 2;

        target = GameObject.FindGameObjectWithTag("Player").transform;
        StopCar = false;
        destroyCar = false;
        interTimer = timer;
    }

    private void Update()
    {
        transform.LookAt(target);
        distance = Vector3.Distance(target.position, this.transform.position);
        Velocity();
        Destroy();

        if (destroyCar == true)
        {
            interTimer -= Time.deltaTime;
        }
        if(interTimer <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    void Velocity()
    {
        if (distance >= furthestRange)
        {
            playerFar = true;
            playerNear = false;
            playerInArea = false;
        }
        else
        {
            if (distance < furthestRange && distance > closestRange)
            {
                playerInArea = true;
                playerNear = false;
                playerFar = false;
            }
            else if (distance <= closestRange)
            {
                playerNear = true;
                playerInArea = false;
                playerFar = false;
            }
        }
        

        if (playerFar == true /*&& detectorFront.gameObject.GetComponent<FrontDetector>().enemyInRange == false*/)
        {
            this.gameObject.GetComponent<RichAI>().maxSpeed = maxVel;
            this.gameObject.GetComponent<RichAI>().acceleration = maxVel / 2f;
        }
        else
        {
            if (playerInArea == true /*&& detectorFront.gameObject.GetComponent<FrontDetector>().enemyInRange == false*/)
            {
                this.gameObject.GetComponent<RichAI>().maxSpeed = normalVel;
                this.gameObject.GetComponent<RichAI>().acceleration = normalVel / 2f;
            }
             else if (playerNear == true /*&& detectorFront.gameObject.GetComponent<FrontDetector>().enemyInRange == false*/)
            {
                this.gameObject.GetComponent<RichAI>().maxSpeed = minVel;
                this.gameObject.GetComponent<RichAI>().acceleration = minVel / 2f;
            }
        }
       

    }

    void Destroy()
    {
        if(StopCar == true )
        {
            maxVel -= 10;
            normalVel -= 10;
            minVel -= 10;
            this.gameObject.GetComponent<RichAI>().maxSpeed -= 10;
            this.gameObject.GetComponent<RichAI>().acceleration -= 10;
            if (StopCar == true && this.gameObject.GetComponent<RichAI>().maxSpeed <= 0)
            {
                //destroyCar = false;
                maxVel = 0;
                normalVel = 0;
                minVel = 0;
                //this.gameObject.GetComponent<RichAI>().rotationSpeed = 0f;
                this.gameObject.GetComponent<RichAI>().maxSpeed = 0;
                this.gameObject.GetComponent<RichAI>().acceleration = 0;
                destroyCar = true;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, closestRange);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, furthestRange);
    }
}
