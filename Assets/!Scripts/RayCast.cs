using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RayCast : MonoBehaviour
{
    public static RayCast instance;

    //public int damage = 10;
    public float range = 100f;
    public LayerMask mask;
    public Camera fpsCam;

    public string target;

    public bool motor, parabrisa, conductor;

    private void Awake()
    {
        instance = this;
        motor = false;
        parabrisa = false;
        conductor = false;

        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = false;
    }

    private void Start()
    {
        //layer_mask = LayerMask.GetMask("EnemyParts");
    }

    private void Update()
    {
        Shoot();
    }


    void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range, mask))
        {
            //Debug.LogError(hit.transform.name);

            target = hit.transform.name;

            //if(hit.transform.name == "Motor")
            //{
            //    motor = true;
            //    parabrisa = false;
            //    conductor = false;
            //}
            //else if(hit.transform.name == "Parabrisa")
            //{
            //    parabrisa = true;
            //    motor = false;
            //    conductor = false;

            //}
            //else if (hit.transform.name == "Conductor")
            //{
            //    conductor = true;
            //    motor = false;
            //    parabrisa = false;
            //}
            //else if(hit.transform.name != "Conductor" || hit.transform.name != "Parabrisa" || hit.transform.name != "Motor")
            //{
            //    conductor = false;
            //    motor = false;
            //    parabrisa = false;
            //}

            switch (target)
            {
                case "Motor":
                    motor = true;
                    parabrisa = false;
                    conductor = false;
                    break;
                case "Parabrisa":
                    parabrisa = true;
                    motor = false;
                    conductor = false;
                    break;
                case "Conductor":
                    conductor = true;
                    motor = false;
                    parabrisa = false;
                    break;
                default:
                    conductor = false;
                    motor = false;
                    parabrisa = false;
                    break;
            }
        }
    }
}
