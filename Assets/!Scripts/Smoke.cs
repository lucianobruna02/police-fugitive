using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smoke : MonoBehaviour
{
    public GameObject smokeWhite;
    public GameObject smokeBlack;
    public ParticleSystem smokeWihte;

    public float timer;
    float inttimer;

    private void Start()
    {
        smokeWhite.SetActive(false);
        smokeBlack.SetActive(false);
        inttimer = timer;
    }
    private void Update()
    {
        if(this.gameObject.GetComponent<PoliceAI>().health == 50)
        {
            ActivateWhiteSmoke();
        }
        if(this.gameObject.GetComponent<PoliceAI>().health <= 30)
        {
            ActivateBlackSmoke();
            inttimer -= Time.deltaTime;
        }

        if (inttimer <= 0)
        {
            smokeWihte.Stop(true);
            inttimer = 1000f;
        }
    }

    void ActivateWhiteSmoke()
    {
        smokeWhite.SetActive(true);
        //smokeWhite.Stop(false);
    }

    void ActivateBlackSmoke()
    {
        smokeBlack.SetActive(true);
        //inttimer -= Time.deltaTime;

        //if (inttimer <= 0)
        //{
        //    smokeWhite.SetActive(false);
        //}
    }
}
