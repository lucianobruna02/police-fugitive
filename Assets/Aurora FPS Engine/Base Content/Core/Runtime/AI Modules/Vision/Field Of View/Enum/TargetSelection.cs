﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using System.Collections.Generic;
using System;
using UnityEngine;
using Math = AuroraFPSRuntime.CoreModules.Mathematics.Math;

namespace AuroraFPSRuntime.AIModules.Vision
{
    public enum TargetSelection
    {
        First,
        Nearest,
        Distant
    }
}