/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright � 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using System.Collections.Generic;

namespace AuroraFPSRuntime.CoreModules.CommandLine
{
    public sealed class CommandHistory
    {
        private readonly List<string> HistoryCache = new List<string>();
        private int position;

        public void Push(string commandString)
        {
            if (commandString == "")
            {
                return;
            }

            HistoryCache.Add(commandString);
            position = HistoryCache.Count;
        }

        public string Next()
        {
            position++;

            if (position >= HistoryCache.Count)
            {
                position = HistoryCache.Count;
                return "";
            }

            return HistoryCache[position];
        }

        public string Previous()
        {
            if (HistoryCache.Count == 0)
            {
                return "";
            }

            position--;

            if (position < 0)
            {
                position = 0;
            }

            return HistoryCache[position];
        }

        public IEnumerable<string> Commands()
        {
            return HistoryCache;
        }

        public int Count()
        {
            return HistoryCache.Count;
        }

        public void Clear()
        {
            HistoryCache.Clear();
            position = 0;
        }
    }
}
