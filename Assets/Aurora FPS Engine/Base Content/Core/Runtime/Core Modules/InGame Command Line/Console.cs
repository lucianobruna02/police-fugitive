/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright ? 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.CoreModules.InputSystem;
using AuroraFPSRuntime.CoreModules.Pattern;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using System;

namespace AuroraFPSRuntime.CoreModules.CommandLine
{
    public enum TerminalState
    {
        Close,
        OpenSmall,
        OpenFull
    }

    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/Core Modules/Developer Tools/Console")]
    [DisallowMultipleComponent]
    public sealed class Console : Singleton<Console>
    {
        [Serializable]
        public class OnSwitchEvent : UnityEvent<TerminalState> { }

        [SerializeField]
        [NotNull]
        private string toggleHotkey = "`";

        [SerializeField]
        [NotNull]
        private string toggleFullHotkey = "#`";

        [SerializeField] 
        [Foldout("Theme Settings", Style = "Header")]
        private Font consoleFont;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        private bool autoResizeFont = true;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        [VisibleIf("autoResizeFont", false)]
        private int fontSize = 15;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        [VisibleIf("autoResizeFont", true)]
        private int fontSizeRatio = 39;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        private string inputCaret = ">";

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        private bool showGUIButtons = false;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        private bool rightAlignButtons = false;

        [SerializeField]
        [Slider(0, 1)]
        [Foldout("Theme Settings", Style = "Header")]
        private float inputContrast = 0.0f;

        [SerializeField]
        [Slider(0, 1)]
        [Foldout("Theme Settings", Style = "Header")]
        private float inputAlpha = 0.5f;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        Color BackgroundColor = Color.black;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        Color ForegroundColor = Color.white;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        Color ShellColor = Color.white;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        Color InputColor = Color.cyan;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        Color WarningColor = Color.yellow;

        [SerializeField]
        [Foldout("Theme Settings", Style = "Header")]
        Color ErrorColor = Color.red;

        [SerializeField]
        [Foldout("Advanced Settings", Style = "Header")]
        private int bufferSize = 512;

        [SerializeField]
        [Slider(0, 1)]
        [Foldout("Advanced Settings", Style = "Header")]
        private float maxHeight = 0.7f;

        [SerializeField]
        [Slider(0, 1)]
        [Foldout("Advanced Settings", Style = "Header")]
        private float smallTerminalRatio = 0.33f;

        [SerializeField]
        [Slider(100, 1000)]
        [Foldout("Advanced Settings", Style = "Header")]
        private float toggleSpeed = 360;

        [SerializeField]
        [Foldout("Event Settings", Style = "Header")]
        private OnSwitchEvent onSwitchEvent;

        [SerializeField]
        [Foldout("Event Settings", Style = "Header")]
        private UnityEvent onOpenEvent;

        [SerializeField]
        [Foldout("Event Settings", Style = "Header")]
        private UnityEvent onOpenFullEvent;

        [SerializeField]
        [Foldout("Event Settings", Style = "Header")]
        private UnityEvent onCloseEvent;

        // Stored required components.
        private InputReceiver inputReceiver;

        // Stored required properties.
        private TerminalState state;
        private TextEditor editorState;
        private bool inputFix;
        private bool moveCursor;
        private bool initialOpen;
        private Rect window;
        private float currentOpen;
        private float openTarget;
        private float realWindowSize;
        private string commandText;
        private string cachedCommandText;
        private Vector2 scrollPosition;
        private Vector2 previousScreenResolution;
        private GUIStyle windowStyle;
        private GUIStyle labelStyle;
        private GUIStyle inputStyle;
        private Texture2D backgroundTexture;
        private Texture2D inputBackgroundTexture;
        private Event toggleKeyEvent;
        private Event fullToggleKeyEvent;
        private Event returnKeyEvent;
        private Event enterKeyEvent;
        private Event tabKeyEvent;
        private Event closeKeyEvent;
        private Event upKeyEvent;
        private Event downKeyEvent;

        public static ConsoleLog Buffer { get; private set; }
        public static CommandShell Shell { get; private set; }
        public static CommandHistory History { get; private set; }
        public static CommandAutoComplete Autocomplete { get; private set; }

        protected override void Awake()
        {
            base.Awake();
            OnSwitchStateCallback += onSwitchEvent.Invoke;
            OnOpenSmallCallback += onOpenEvent.Invoke;
            OnOpenFullCallback += onOpenFullEvent.Invoke;
            OnCloseCallback += onCloseEvent.Invoke;
        }

        public static bool IssuedError
        {
            get { return Shell.GetIssuedErrorMessage() != null; }
        }

        public bool IsClosed
        {
            get { return state == TerminalState.Close && Mathf.Approximately(currentOpen, openTarget); }
        }

        public static void Log(string format, params object[] message)
        {
            Log(ConsoleLog.LogType.ShellMessage, format, message);
        }

        public static void Log(ConsoleLog.LogType type, string format, params object[] message)
        {
            Buffer.HandleLog(string.Format(format, message), type);
        }

        public void SetState(TerminalState newState)
        {
            inputFix = true;
            cachedCommandText = commandText;
            commandText = "";

            switch (newState)
            {
                case TerminalState.Close:
                    {
                        openTarget = 0;
                        OnCloseCallback?.Invoke();
                        break;
                    }
                case TerminalState.OpenSmall:
                    {
                        openTarget = Screen.height * maxHeight * smallTerminalRatio;
                        if (currentOpen > openTarget)
                        {
                            // Prevent resizing from OpenFull to OpenSmall if window y position
                            // is greater than OpenSmall's target
                            openTarget = 0;
                            state = TerminalState.Close;
                            return;
                        }
                        realWindowSize = openTarget;
                        scrollPosition.y = int.MaxValue;
                        OnOpenSmallCallback?.Invoke();
                        break;
                    }
                case TerminalState.OpenFull:
                default:
                    {
                        realWindowSize = Screen.height * maxHeight;
                        openTarget = realWindowSize;
                        OnOpenFullCallback?.Invoke();
                        break;
                    }
            }
            state = newState;
            OnSwitchStateCallback?.Invoke(state);
        }

        public void ToggleState(TerminalState newState)
        {
            if (state == newState)
            {
                SetState(TerminalState.Close);
            }
            else
            {
                SetState(newState);
            }
        }


        private void OnEnable()
        {
            Buffer = new ConsoleLog(bufferSize);
            Shell = new CommandShell();
            History = new CommandHistory();
            Autocomplete = new CommandAutoComplete();

            // Hook Unity log events
            Application.logMessageReceivedThreaded += HandleUnityLog;
        }

        private void OnDisable()
        {
            Application.logMessageReceivedThreaded -= HandleUnityLog;
        }

        private void Start()
        {
            inputReceiver = InputReceiver.GetRuntimeInstance();

            previousScreenResolution = new Vector2(Screen.width, Screen.height);

            if (autoResizeFont)
            {
                fontSize = Mathf.Min(Screen.width, Screen.height) / fontSizeRatio;
            }

            if (consoleFont == null)
            {
                consoleFont = Font.CreateDynamicFontFromOSFont("Courier New", fontSize);
            }

            commandText = "";
            cachedCommandText = commandText;
            Assert.AreNotEqual(toggleHotkey.ToLower(), "return", "Return is not a valid ToggleHotkey");

            SetupWindow();
            SetupInput();
            SetupLabels();

            Shell.RegisterCommands();

            if (IssuedError)
            {
                Log(ConsoleLog.LogType.Error, "Error: {0}", Shell.GetIssuedErrorMessage());
            }

            foreach (var command in Shell.GetCommands())
            {
                Autocomplete.Register(command.Key);
            }

            toggleKeyEvent = Event.KeyboardEvent(toggleHotkey);
            fullToggleKeyEvent = Event.KeyboardEvent(toggleFullHotkey);
            returnKeyEvent = Event.KeyboardEvent("return");
            enterKeyEvent = Event.KeyboardEvent("[enter]");
            tabKeyEvent = Event.KeyboardEvent("tab");
            closeKeyEvent = Event.KeyboardEvent("`");
            upKeyEvent = Event.KeyboardEvent("up");
            downKeyEvent = Event.KeyboardEvent("down");
        }

        private void OnGUI()
        {
            if (autoResizeFont && (previousScreenResolution.x != Screen.width || previousScreenResolution.y != Screen.height))
            {
                fontSize = Mathf.Min(Screen.width, Screen.height) / fontSizeRatio;
                previousScreenResolution.x = Screen.width;
                previousScreenResolution.y = Screen.height;
                inputStyle.fontSize = fontSize;
                inputStyle.fixedHeight = fontSize * 1.6f;
                labelStyle.fontSize = fontSize;
                windowStyle.fontSize = fontSize;
            }

            Event current = Event.current;
            if (current.Equals(toggleKeyEvent))
            {
                SetState(TerminalState.OpenSmall);
                initialOpen = true;
            }
            else if (current.Equals(fullToggleKeyEvent))
            {
                SetState(TerminalState.OpenFull);
                initialOpen = true;
            }

            if (showGUIButtons)
            {
                DrawGUIButtons();
            }

            if (IsClosed)
            {
                return;
            }

            HandleOpenness();
            window = GUILayout.Window(88, window, DrawConsole, "", windowStyle);
        }

        private void SetupWindow()
        {
            realWindowSize = Screen.height * maxHeight / 3;
            window = new Rect(0, currentOpen - realWindowSize, Screen.width, realWindowSize);

            // Set background color
            backgroundTexture = new Texture2D(1, 1);
            backgroundTexture.SetPixel(0, 0, BackgroundColor);
            backgroundTexture.Apply();

            windowStyle = new GUIStyle();
            windowStyle.normal.background = backgroundTexture;
            windowStyle.padding = new RectOffset(4, 4, 4, 4);
            windowStyle.normal.textColor = ForegroundColor;
            windowStyle.font = consoleFont;
            windowStyle.fontSize = fontSize;
        }

        private void SetupLabels()
        {
            labelStyle = new GUIStyle();
            labelStyle.font = consoleFont;
            labelStyle.fontSize = fontSize;
            labelStyle.normal.textColor = ForegroundColor;
            labelStyle.wordWrap = true;
        }

        private void SetupInput()
        {
            inputStyle = new GUIStyle();
            inputStyle.padding = new RectOffset(4, 4, 4, 4);
            inputStyle.fontSize = fontSize;
            inputStyle.font = consoleFont;
            inputStyle.fixedHeight = fontSize * 1.6f;
            inputStyle.normal.textColor = InputColor;

            var dark_background = new Color();
            dark_background.r = BackgroundColor.r - inputContrast;
            dark_background.g = BackgroundColor.g - inputContrast;
            dark_background.b = BackgroundColor.b - inputContrast;
            dark_background.a = inputAlpha;

            inputBackgroundTexture = new Texture2D(1, 1);
            inputBackgroundTexture.SetPixel(0, 0, dark_background);
            inputBackgroundTexture.Apply();
            inputStyle.normal.background = inputBackgroundTexture;
        }

        private void DrawConsole(int Window2D)
        {
            GUILayout.BeginVertical();

            scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false, GUIStyle.none, GUIStyle.none);
            GUILayout.FlexibleSpace();
            DrawLogs();
            GUILayout.EndScrollView();

            if (moveCursor)
            {
                CursorToEnd();
                moveCursor = false;
            }

            Event current = Event.current;
            if (current.Equals(closeKeyEvent))
            {
                SetState(TerminalState.Close);
            }
            else if (current.Equals(returnKeyEvent)
              || current.Equals(enterKeyEvent))
            {
                EnterCommand();
            }
            else if (current.Equals(upKeyEvent))
            {
                commandText = History.Previous();
                moveCursor = true;
            }
            else if (current.Equals(downKeyEvent))
            {
                commandText = History.Next();
            }
            else if (current.Equals(toggleHotkey))
            {
                ToggleState(TerminalState.OpenSmall);
            }
            else if (current.Equals(toggleFullHotkey))
            {
                ToggleState(TerminalState.OpenFull);
            }
            else if (current.Equals(tabKeyEvent))
            {
                CompleteCommand();
                moveCursor = true; // Wait till next draw call
            }

            GUILayout.BeginHorizontal();

            if (inputCaret != "")
            {
                GUILayout.Label(inputCaret, inputStyle, GUILayout.Width(fontSize));
            }

            GUI.SetNextControlName("command_text_field");
            commandText = GUILayout.TextField(commandText, inputStyle);

            if (inputFix && commandText.Length > 0)
            {
                commandText = cachedCommandText; // Otherwise the TextField picks up the ToggleHotkey character event
                inputFix = false;                  // Prevents checking string Length every draw call
            }

            if (initialOpen)
            {
                GUI.FocusControl("command_text_field");
                initialOpen = false;
            }

            if (showGUIButtons && GUILayout.Button("| run", inputStyle, GUILayout.Width(Screen.width / 10)))
            {
                EnterCommand();
            }

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        private void DrawLogs()
        {
            foreach (var log in Buffer.Logs)
            {
                labelStyle.normal.textColor = GetLogColor(log.type);
                GUILayout.Label(log.message, labelStyle);
            }
        }

        private void DrawGUIButtons()
        {
            int size = fontSize;
            float x_position = rightAlignButtons ? Screen.width - 7 * size : 0;

            // 7 is the number of chars in the button plus some padding, 2 is the line height.
            // The layout will resize according to the font size.
            GUILayout.BeginArea(new Rect(x_position, currentOpen, 7 * size, size * 2));
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Small", windowStyle))
            {
                ToggleState(TerminalState.OpenSmall);
            }
            else if (GUILayout.Button("Full", windowStyle))
            {
                ToggleState(TerminalState.OpenFull);
            }

            GUILayout.EndHorizontal();
            GUILayout.EndArea();
        }

        private void HandleOpenness()
        {
            float dt = toggleSpeed * Time.unscaledDeltaTime;

            if (currentOpen < openTarget)
            {
                currentOpen += dt;
                if (currentOpen > openTarget) currentOpen = openTarget;
            }
            else if (currentOpen > openTarget)
            {
                currentOpen -= dt;
                if (currentOpen < openTarget) currentOpen = openTarget;
            }
            else
            {
                if (inputFix)
                {
                    inputFix = false;
                }
                return; // Already at target
            }

            window = new Rect(0, currentOpen - realWindowSize, Screen.width, realWindowSize);
        }

        private void EnterCommand()
        {
            Log(ConsoleLog.LogType.Input, "{0} {1}", inputCaret, commandText);
            Shell.RunCommand(commandText);
            History.Push(commandText);

            if (IssuedError)
            {
                Log(ConsoleLog.LogType.Error, "Error: {0}", Shell.GetIssuedErrorMessage());
            }

            commandText = "";
            scrollPosition.y = int.MaxValue;
        }

        private void CompleteCommand()
        {
            string head_text = commandText;
            int format_width = 0;

            string[] completion_buffer = Autocomplete.Complete(ref head_text, ref format_width);
            int completion_length = completion_buffer.Length;

            if (completion_length != 0)
            {
                commandText = head_text;
            }

            if (completion_length > 1)
            {
                // Print possible completions
                var log_buffer = new StringBuilder();

                foreach (string completion in completion_buffer)
                {
                    log_buffer.Append(completion.PadRight(format_width + 4));
                }

                Log("{0}", log_buffer);
                scrollPosition.y = int.MaxValue;
            }
        }

        private void CursorToEnd()
        {
            if (editorState == null)
            {
                editorState = (TextEditor)GUIUtility.GetStateObject(typeof(TextEditor), GUIUtility.keyboardControl);
            }

            editorState.MoveCursorToPosition(new Vector2(999, 999));
        }

        private void HandleUnityLog(string message, string stack_trace, LogType type)
        {
            Buffer.HandleLog(message, stack_trace, (ConsoleLog.LogType)type);
            scrollPosition.y = int.MaxValue;
        }

        private Color GetLogColor(ConsoleLog.LogType type)
        {
            switch (type)
            {
                case ConsoleLog.LogType.Message: return ForegroundColor;
                case ConsoleLog.LogType.Warning: return WarningColor;
                case ConsoleLog.LogType.Input: return InputColor;
                case ConsoleLog.LogType.ShellMessage: return ShellColor;
                default: return ErrorColor;
            }
        }

        public void Open()
        {
            SetState(TerminalState.OpenSmall);
        }

        public void OpenFull()
        {
            SetState(TerminalState.OpenFull);
        }

        public void Close()
        {
            SetState(TerminalState.Close);
        }

        #region [Event Callback Function]
        /// <summary>
        /// Called when console state was changed.
        /// </summary>
        public event Action<TerminalState> OnSwitchStateCallback;

        /// <summary>
        /// Called when terminal being opened as small.
        /// </summary>
        public event Action OnOpenSmallCallback;

        /// <summary>
        /// Called when terminal being opened as full.
        /// </summary>
        public event Action OnOpenFullCallback;

        /// <summary>
        /// Called when terminal being closed.
        /// </summary>
        public event Action OnCloseCallback;
        #endregion
    }
}
