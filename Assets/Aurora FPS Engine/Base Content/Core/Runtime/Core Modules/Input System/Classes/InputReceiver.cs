﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.CoreModules.Pattern;
using UnityEngine;
using UnityEngine.InputSystem;

namespace AuroraFPSRuntime.CoreModules.InputSystem
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Input/Input Receiver")]
    [DisallowMultipleComponent]
	public sealed class InputReceiver : Singleton<InputReceiver>
    {
		[SerializeField]
        [NotNull]
		private InputActionAsset inputActions;

        [SerializeField]
        [Foldout("Advanced Settings", Style = "Header")]
        private bool hardwareCursor = false;

        // Stored required properties.
        private InputActionMap inputActionMap;
		private InputAction movementVerticalAction;
		private InputAction movementHorizontalAction;
		private InputAction cameraVerticalAction;
		private InputAction cameraHorizontalAction;
        private InputAction scrollInventoryItemsAction;
		private InputAction jumpAction;
		private InputAction crouchAction;
		private InputAction zoomAction;
		private InputAction lightWalkAction;
		private InputAction sprintAction;
        private InputAction attackAction;
        private InputAction reloadAction;
        private InputAction changeFireModeAction;
        private InputAction hideWeaponAction;
        private InputAction tossWeaponAction;
        private InputAction grabAction;
        private InputAction throwAction;
        private InputAction interactAction;

        /// <summary>
        /// Called when the script instance is being loaded.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            if(inputActions == null)
            {
                TryLoadDefaultMap();
            }
            Refresh();
        }

        /// <summary>
        /// Called on the frame when a script is enabled,
        /// just before any of the Update methods are called the first time.
        /// </summary>
        private void Start()
        {
            inputActions.Enable();
            HardwareCursor(hardwareCursor);
        }

        /// <summary>
        /// Called when the object becomes enabled and active.
        /// </summary>
        private void OnEnable()
        {
            inputActionMap.Enable();
        }

        /// <summary>
        /// Called when the behaviour becomes disabled.
        /// </summary>
        private void OnDisable()
        {
            inputActionMap.Disable();
        }

        /// <summary>
        /// Refresh input receiver.
        /// </summary>
        public void Refresh()
        {
            inputActionMap = inputActions.FindActionMap("Player", true);
            movementVerticalAction = inputActionMap.FindAction("Movement Vertical", true);
            movementHorizontalAction = inputActionMap.FindAction("Movement Horizontal", true);
            cameraVerticalAction = inputActionMap.FindAction("Camera Vertical", true);
            cameraHorizontalAction = inputActionMap.FindAction("Camera Horizontal", true);
            scrollInventoryItemsAction = inputActionMap.FindAction("Scroll Inventory Items", true);
            jumpAction = inputActionMap.FindAction("Jump", true);
            crouchAction = inputActionMap.FindAction("Crouch", true);
            zoomAction = inputActionMap.FindAction("Zoom", true);
            lightWalkAction = inputActionMap.FindAction("Light Walk", true);
            sprintAction = inputActionMap.FindAction("Sprint", true);
            attackAction = inputActionMap.FindAction("Attack", true);
            reloadAction = inputActionMap.FindAction("Reload", true);
            changeFireModeAction = inputActionMap.FindAction("Change Fire Mode", true);
            hideWeaponAction = inputActionMap.FindAction("Hide Weapon", true);
            tossWeaponAction = inputActionMap.FindAction("Toss Weapon", true);
            grabAction = inputActionMap.FindAction("Grab", true);
            throwAction = inputActionMap.FindAction("Throw", true);
            interactAction = inputActionMap.FindAction("Interact", true);
        }

        /// <summary>
        /// Enable input receiver.
        /// </summary>
        /// <param name="onlyPlayer">Enable input actions which controlled only by Player receiver.</param>
        [System.Obsolete("Use EnableMap(string map) or EnableAction(string <path>) instead.")]
        public void Enable(bool onlyPlayer = true)
        {
            if (onlyPlayer)
            {
                inputActionMap.Enable();
            }
            else
            {
                inputActions.Enable();
            }
        }

        /// <summary>
        /// Disable input receiver.
        /// </summary>
        /// <param name="onlyPlayer">Disable input actions which controlled only by Player receiver.</param>
        [System.Obsolete("Use DisableMap(string map) or DisableAction(string <path>) instead.")]
        public void Disable(bool onlyPlayer = true)
        {
            if (onlyPlayer)
            {
                inputActionMap.Disable();
            }
            else
            {
                inputActions.Disable();
            }
        }

        public void EnableMap(string name)
        {
            InputActionMap actionMap = inputActions.FindActionMap(name, false);
            actionMap?.Enable();
        }

        public void DisableMap(string name)
        {
            InputActionMap actionMap = inputActions.FindActionMap(name, false);
            actionMap?.Disable();
        }

        public void EnableAction(string path)
        {
            InputAction action = inputActions.FindAction(path, false);
            action?.Enable();
        }

        public void DisableDisable(string path)
        {
            InputAction action = inputActions.FindAction(path, false);
            action?.Disable();
        }

        public InputAction FindAction(string name)
        {
            return inputActions.FindAction(name, true);
        }

        private void TryLoadDefaultMap()
        {
            inputActions = Resources.Load<InputActionAsset>("Input/InputMap");
        }

        #region [Getter / Setter]
        public InputActionAsset GetInputActionAsset()
        {
            return inputActions;
        }

        public void SetInputActionAsset(InputActionAsset value)
        {
            inputActions = value;
        }

        public bool HardwareCursor()
        {
            return hardwareCursor;
        }

        public void HardwareCursor(bool value)
        {
            hardwareCursor = value;
            if (hardwareCursor)
            {
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }

        public InputAction GetMovementVerticalAction()
        {
            if(movementVerticalAction == null)
            {
                if(inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                movementVerticalAction = inputActionMap.FindAction("Movement Vertical", true);
            }
			return movementVerticalAction;
        }

        public InputAction GetMovementHozrizontalAction()
        {
            if (movementHorizontalAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                movementHorizontalAction = inputActionMap.FindAction("Movement Horizontal", true);
            }
            return movementHorizontalAction;
        }

		public InputAction GetCameraVerticalAction()
        {
            if (cameraVerticalAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                cameraVerticalAction = inputActionMap.FindAction("Camera Vertical", true);
            }
            return cameraVerticalAction;
        }

        public InputAction GetCameraHorizontalAction()
        {
            if (cameraHorizontalAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                cameraHorizontalAction = inputActionMap.FindAction("Camera Horizontal", true);
            }
            return cameraHorizontalAction;
        }

        public InputAction GetScrollInventoryItemsAction()
        {
            if (scrollInventoryItemsAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                scrollInventoryItemsAction = inputActionMap.FindAction("Scroll Inventory Items", true);
            }
            return scrollInventoryItemsAction;
        }

        public InputAction GetJumpAction()
        {
            if (jumpAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                jumpAction = inputActionMap.FindAction("Jump", true);
            }
            return jumpAction;
        }

        public InputAction GetCrouchAction()
        {
            if (crouchAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                crouchAction = inputActionMap.FindAction("Crouch", true);
            }
            return crouchAction;
        }

        public InputAction GetZoomAction()
        {
            if (zoomAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                zoomAction = inputActionMap.FindAction("Zoom", true);
            }
            return zoomAction;
        }

        public InputAction GetLightWalkAction()
        {
            if (lightWalkAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                lightWalkAction = inputActionMap.FindAction("Light Walk", true);
            }
            return lightWalkAction;
        }

        public InputAction GetSprintAction()
        {
            if (sprintAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                sprintAction = inputActionMap.FindAction("Sprint", true);
            }
            return sprintAction;
        }

        public InputAction GetAttackAction()
        {
            if (attackAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                attackAction = inputActionMap.FindAction("Attack", true);
            }
            return attackAction;
        }

        public InputAction GetReloadAction()
        {
            if (reloadAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                reloadAction = inputActionMap.FindAction("Reload", true);
            }
            return reloadAction;
        }

        public InputAction GetChangeFireModeAction()
        {
            if (changeFireModeAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                changeFireModeAction = inputActionMap.FindAction("Change Fire Mode", true);
            }
            return changeFireModeAction;
        }

        public InputAction GetHideWeaponAction()
        {
            if (hideWeaponAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                hideWeaponAction = inputActionMap.FindAction("Hide Weapon", true);
            }
            return hideWeaponAction;
        }

        public InputAction GetTossWeaponAction()
        {
            if (tossWeaponAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                tossWeaponAction = inputActionMap.FindAction("Toss Weapon", true);
            }
            return tossWeaponAction;
        }

        public InputAction GetGrabAction()
        {
            if (grabAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                grabAction = inputActionMap.FindAction("Grab", true);
            }
            return grabAction;
        }

        public InputAction GetThrowAction()
        {
            if (throwAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                throwAction = inputActionMap.FindAction("Throw", true);
            }
            return throwAction;
        }

        public InputAction GetInteractAction() 
        {
            if (interactAction == null)
            {
                if (inputActionMap == null)
                {
                    if (inputActions == null)
                    {
                        TryLoadDefaultMap();
                    }
                    inputActionMap = inputActions.FindActionMap("Player", true);
                }
                interactAction = inputActionMap.FindAction("Interact", true);
            }
            return interactAction;
        }
        #endregion
    }
}