﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using System;

namespace AuroraFPSRuntime.SystemModules
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class CameraEffectMenuAttribute : Attribute
    {
        public readonly string name;
        public readonly string path;

        public CameraEffectMenuAttribute(string name, string path)
        {
            this.name = name;
            this.path = path;
        }

        #region [Optional]
        public bool Hide { get; set; }
        #endregion
    }
}