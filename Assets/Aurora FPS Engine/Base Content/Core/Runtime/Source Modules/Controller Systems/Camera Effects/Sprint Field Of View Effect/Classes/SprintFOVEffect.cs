﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules.ControllerModules
{
    [System.Serializable]
    [CameraEffectMenu("Sprinting Field Of View", "Field Of View/Sprint Effect")]
    public sealed class SprintFOVEffect : CameraEffect
    {
        [SerializeField]
        [HideExpandButton]
        private FieldOfViewSettings sprintFOV = new FieldOfViewSettings(10, 0.5f, AnimationCurve.Linear(0, 0, 1, 1), true);

        // Stored required components.
        private Controller controller;
        private FPCameraControl cameraControl;

        // Stored required properties.
        private bool isSprintFOV;

        public override void Initialization(Controller controller, FPCameraControl cameraControl)
        {
            this.controller = controller;
            this.cameraControl = cameraControl;

            this.controller.OnMoveCallback += OnMoveCallback;
        }

        /// <summary>
        /// Called while controller is moving.
        /// </summary>
        /// <param name="velocity">Current controller movement velocity.</param>
        private void OnMoveCallback(Vector3 velocity)
        {
            if (controller.HasState(ControllerState.Sprinting) & !isSprintFOV)
            {
                cameraControl.ChangeFieldOfView(sprintFOV, true);
                isSprintFOV = true;
            }
            else if(!controller.HasState(ControllerState.Sprinting) & isSprintFOV)
            {
                cameraControl.ChangeFieldOfView(cameraControl.GetDefaultFOVSettings(), true);
                isSprintFOV = false;
            }
        }
    }
}