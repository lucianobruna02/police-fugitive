﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.SystemModules.HealthModules;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Looting/Aid Kit")]
    [DisallowMultipleComponent]
    public class AidKit : InteractiveObject
    {
        [SerializeField]
        [MinValue(0.0f)]
        private float healthPoint = 100;

        public override bool Execute(Transform other)
        {
            ObjectHealth health = other.GetComponent<ObjectHealth>();
            if(health != null && health.IsAlive())
            {
                health.ApplyHealth(healthPoint);
                Push();
                return true;
            }
            return false;
        }

        protected override void CalculateMessageCode(Transform other, out int messageCode)
        {
            ObjectHealth health = other.GetComponent<ObjectHealth>();
            if (health != null && health.IsAlive() && health.GetHealth() < health.GetMaxHealth())
            {
                messageCode = 1;
            }
            else
            {
                messageCode = 0;
            }
        }
    }
}
