﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.SystemModules.InventoryModules;
using AuroraFPSRuntime.WeaponModules;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Looting/Loot Weapon")]
    [DisallowMultipleComponent]
    public sealed class LootWeapon : InteractiveObject
    {
        [SerializeField]
        [NotNull]
        private EquippableItem item;

        public override bool Execute(Transform other)
        {
            int messageCode = GetMessageCode();
            WeaponInventorySystem inventorySystem = other.GetComponent<WeaponInventorySystem>();
            if (inventorySystem != null)
            {
                switch (messageCode)
                {
                    case 1:
                        if (inventorySystem.AddItem(item))
                        {
                            inventorySystem.UseItem(item);
                            Push();
                            return true;
                        }
                        return false;
                    case 2:
                        if (inventorySystem.SwapItem(item))
                        {
                            Push();
                            return true;
                        }
                        return false;
                }
            }
            return false;
        }

        protected override void CalculateMessageCode(Transform other, out int messageCode)
        {
            messageCode = 0;
            WeaponInventorySystem inventorySystem = other.GetComponent<WeaponInventorySystem>();
            if (inventorySystem != null)
            {
                if (inventorySystem.HasSpace(item.GetItemType()))
                {
                    messageCode = 1;
                }
                else
                {
                    messageCode = 2;
                }
            }
        }
    }
}