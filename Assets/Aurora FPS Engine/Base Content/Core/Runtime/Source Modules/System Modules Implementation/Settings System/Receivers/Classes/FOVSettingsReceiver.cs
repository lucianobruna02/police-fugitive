﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Aurora FPS Engine
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Shakirov
   ---------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using UnityEngine;
using AuroraFPSRuntime.SystemModules.ControllerModules;

namespace AuroraFPSRuntime.SystemModules.SettingsSystem
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Settings System/Receivers/Field Of View Settings Receiver")]
    [DisallowMultipleComponent]
    public sealed class FOVSettingsReceiver : SettingsReceiver
    {
        [SerializeField]
        [NotNull]
        private Controller controller;

        [SerializeField]
        [Slider(0.0f, 179.0f)]
        private float defaultValue = 85.0f;

        /// <summary>
        /// <br>Called when the settings manager load the settings file.</br>
        /// <br>Note: Called only if selected stream <i>Load</i> option. Otherwise this callback will be ignored.</br>
        /// <br>Implement this method to load processor value.</br>
        /// </summary>
        protected override void OnLoad(object value)
        {
            float fieldOfView = (float)value;

            controller.GetCameraControl().GetCamera().fieldOfView = fieldOfView;

            FieldOfViewSettings fovSettings = controller.GetCameraControl().GetDefaultFOVSettings();
            fovSettings.SetValue(fieldOfView);
            controller.GetCameraControl().SetDefaultFOVSettings(fovSettings);
        }

        /// <summary>
        /// <br>Called when settings file is not found or 
        /// target processor GUID is not found in loaded buffer.</br>
        /// <br>Implement this method to determine default value for this processor.</br>
        /// </summary>
        /// <returns>Default value of processor.</returns>
        public override object GetDefaultValue()
        {
            return defaultValue;
        }
    }
}