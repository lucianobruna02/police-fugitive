﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Aurora FPS Engine
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Shakirov
   ---------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using UnityEngine;
using AuroraFPSRuntime.SystemModules.ControllerModules;

namespace AuroraFPSRuntime.SystemModules.SettingsSystem
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Settings System/Receivers/Invert Vertical Rotation Settings Receiver")]
    [DisallowMultipleComponent]
    public sealed class InvertVerticalRotationSettingsReceiver : SettingsReceiver
    {
        [SerializeField]
        [NotNull]
        private Controller controller;

        [SerializeField]
        private bool defaultValue = false;

        /// <summary>
        /// <br>Called when the settings manager load the settings file.</br>
        /// <br>Implement this method to load processor value.</br>
        /// </summary>
        protected override void OnLoad(object value)
        {
            bool invert = (bool)value;
            controller.GetCameraControl().InvertRotation(controller.GetCameraControl().IsHorizontalRotationInverted(), invert);
        }

        /// <summary>
        /// <br>Called when settings file is not found or 
        /// target processor GUID is not found in loaded buffer.</br>
        /// <br>Implement this method to determine default value for this processor.</br>
        /// </summary>
        /// <returns>Default value of processor.</returns>
        public override object GetDefaultValue()
        {
            return defaultValue;
        }
    }
}