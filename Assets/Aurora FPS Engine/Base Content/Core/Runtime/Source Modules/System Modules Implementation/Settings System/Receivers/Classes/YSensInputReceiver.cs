﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Aurora FPS Engine
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Shakirov
   ---------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using UnityEngine;
using AuroraFPSRuntime.SystemModules.ControllerModules;

namespace AuroraFPSRuntime.SystemModules.SettingsSystem
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Settings System/Receivers/Y Sens Input Settings Receiver")]
    [DisallowMultipleComponent]
    public sealed class YSensInputReceiver : SettingsReceiver
    {
        [SerializeField]
        [NotNull]
        private Controller controller;

        [SerializeField]
        [MinValue(0.0f)]
        private float defaultValue = 15.0f;

        /// <summary>
        /// <br>Called when the settings manager load the settings file.</br>
        /// <br>Note: Called only if selected stream <i>Load</i> option. Otherwise this callback will be ignored.</br>
        /// <br>Implement this method to load processor value.</br>
        /// </summary>
        protected override void OnLoad(object value)
        {
            Vector2 sensitivity = controller.GetCameraControl().GetSensitivity();
            sensitivity.y = (float)value;
            controller.GetCameraControl().SetSensitivity(sensitivity);
        }

        /// <summary>
        /// <br>Called when settings file is not found or 
        /// target processor GUID is not found in loaded buffer.</br>
        /// <br>Implement this method to determine default value for this processor.</br>
        /// </summary>
        /// <returns>Default value of processor.</returns>
        public override object GetDefaultValue()
        {
            return defaultValue;
        }
    }
}