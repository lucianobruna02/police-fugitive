﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.CoreModules;
using AuroraFPSRuntime.CoreModules.ValueTypes;
using AuroraFPSRuntime.SystemModules.ControllerModules;
using UnityEngine;
using AuroraFPSRuntime.SystemModules;

#region [Unity Editor Section]
#if UNITY_EDITOR
using UnityEditor;
#endif
#endregion

namespace AuroraFPSRuntime.WeaponModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/Weapon Modules/Animation/Weapon Animator System")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public sealed class WeaponAnimatorSystem : EquippableObjectAnimationSystem
    {
        [System.Serializable]
        public class StateEvent : CallbackEvent<AnimatorState> { }

        [SerializeField]
        [Foldout("Default States", Style = "Header")]
        private AnimatorState fireState = "Fire";

        [SerializeField]
        [Foldout("Default States", Style = "Header")]
        private AnimatorState dryFireState = "Dry Fire";

        [SerializeField]
        [Foldout("Default States", Style = "Header")]
        private AnimatorState fireZoomState = "Zoom Fire";

        [SerializeField]
        [Foldout("Default States", Style = "Header")]
        private AnimatorState dryFireZoomState = "Zoom Dry Fire";

        [SerializeField]
        [Foldout("Default States", Style = "Header")]
        private AnimatorState selectState = "Select";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter speedParameter = "Speed";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter isGroundedParameter = "IsGrounded";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter isCrouchingParameter = "IsCrouching";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter isSprintingParameter = "IsSprinting";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter isZoomingParameter = "IsZooming";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter hideParameter = "Hide";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter isJumpedParameter = "IsJumped";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter isLandedParameter = "IsLanded";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter reloadClipMultiplierParameter = "ReloadClipMultiplier";

        [SerializeField]
        [InlineButton("OnSetParameterCallback", Label = "@align_vertically_center", Style = "IconButton")]
        [Foldout("Default Parameters", Style = "Header")]
        private AnimatorParameter reloadMultiplierParameter = "ReloadMultiplier";

        [SerializeField]
        [Foldout("Override State Speed", Style = "Header")]
        [MinValue(0.0f)]
        private float reloadClipMultiplier = 1.0f;

        [SerializeField]
        [Foldout("Override State Speed", Style = "Header")]
        [MinValue(0.0f)]
        private float reloadMultiplier = 1.0f;

        [SerializeField]
        [ReorderableList(DisplayHeader = false, DisplaySeparator = true)]
        [Foldout("Custom State Events", Style = "Header")]
        private StateEvent[] stateEvents;

        // Stored required components.
        private Animator animator;
        private Controller controller;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            animator = GetComponent<Animator>();
            controller = GetComponentInParent<Controller>();

            FPCameraControl cameraControl = controller.GetCameraControl();

            WeaponShootingSystem weaponShootingSystem = GetComponent<WeaponShootingSystem>();
            if (weaponShootingSystem != null)
            {
                weaponShootingSystem.OnFireCallback += () =>
                {
                    if (!cameraControl.IsZooming())
                    {
                        PlayAnimationState(fireState);
                    }
                    else
                    {
                        PlayAnimationState(fireZoomState);
                    }
                };

                weaponShootingSystem.OnDryFireCallback += () =>
                {
                    if (!cameraControl.IsZooming())
                    {
                        PlayAnimationState(dryFireState);
                    }
                    else
                    {
                        PlayAnimationState(dryFireZoomState);
                    }
                };
            }

            animator.SetFloat(reloadClipMultiplierParameter, reloadClipMultiplier);
            animator.SetFloat(reloadMultiplierParameter, reloadMultiplier);
        }

        /// <summary>
        /// Start is called on the frame when a script is enabled 
        /// just before any of the Update methods are called the first time.
        /// </summary>
        private void Start()
        {
            RegisterStateEvents();
            controller.OnBecomeAirCallback += () => animator.SetTrigger(isJumpedParameter);
            controller.OnGroundedCallback += () => animator.SetTrigger(isLandedParameter);
            controller.GetCameraControl().OnStopZoomCallback += () => 
            {
                animator.ResetTrigger(isJumpedParameter);
                animator.ResetTrigger(isLandedParameter);
            };
        }

        private void RegisterStateEvents()
        {
            for (int i = 0; i < stateEvents.Length; i++)
            {
                stateEvents[i].RegisterCallback(PlayAnimationState);
            }
        }

        private void PlayAnimationState(AnimatorState state)
        {
            animator.CrossFadeInFixedTime(state);
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            animator.SetFloat(speedParameter, controller.GetSpeed());
            animator.SetBool(isGroundedParameter, controller.IsGrounded());
            animator.SetBool(isCrouchingParameter, controller.IsCrouched());
            animator.SetBool(isSprintingParameter, controller.HasState(ControllerState.Sprinting));
            animator.SetBool(isZoomingParameter, controller.GetCameraControl().IsZooming());
        }

        /// <summary>
        /// Empty implementation.
        /// Weapon pull animation playing automatically as entry state in animator.
        /// </summary>
        public override void PlayPullAnimation() 
        {
            animator.CrossFadeInFixedTime(selectState);
        }

        public override void PlayPushAnimation()
        {
            animator.SetTrigger(hideParameter);
        }

        #region [Unity Editor Section]
#if UNITY_EDITOR
        private void OnSetParameterCallback(SerializedProperty property)
        {
            if(animator == null)
            {
                animator = GetComponent<Animator>();
            }

            GenericMenu genericMenu = new GenericMenu();
            for (int i = 0; i < animator.parameterCount; i++)
            {
                AnimatorControllerParameter parameter = animator.parameters[i];
                genericMenu.AddItem(new GUIContent(parameter.name), false, () =>
                {
                    property.FindPropertyRelative("name").stringValue = parameter.name;
                    property.FindPropertyRelative("nameHash").intValue = parameter.nameHash;
                    property.serializedObject.ApplyModifiedProperties();
                });
            }
            genericMenu.ShowAsContext();
        }
#endif
#endregion

        #region [Getter / Setter]
        public StateEvent[] GetStateEvents()
        {
            return stateEvents;
        }

        public void SetStateEvents(StateEvent[] value)
        {
            stateEvents = value;
        }

        public AnimatorParameter GetSpeedParameter()
        {
            return speedParameter;
        }

        public void SetSpeedParameter(AnimatorParameter value)
        {
            speedParameter = value;
        }

        public AnimatorParameter GetIsGroundedParameter()
        {
            return isGroundedParameter;
        }

        public void SetIsGroundedParameter(AnimatorParameter value)
        {
            isGroundedParameter = value;
        }

        public AnimatorParameter GetIsCrouchingParameter()
        {
            return isCrouchingParameter;
        }

        public void SetIsCrouchingParameter(AnimatorParameter value)
        {
            isCrouchingParameter = value;
        }

        public AnimatorParameter GetIsSprintingParameter()
        {
            return isSprintingParameter;
        }

        public void SetIsSprintingParameter(AnimatorParameter value)
        {
            isSprintingParameter = value;
        }

        public AnimatorParameter GetHideParameter()
        {
            return hideParameter;
        }

        public void SetHideParameter(AnimatorParameter value)
        {
            hideParameter = value;
        }

        public AnimatorParameter GetReloadClipMultiplierParameter()
        {
            return reloadClipMultiplierParameter;
        }

        public void SetReloadClipMultiplierParameter(AnimatorParameter value)
        {
            reloadClipMultiplierParameter = value;
        }

        public AnimatorParameter GetReloadMultiplierParameter()
        {
            return reloadMultiplierParameter;
        }

        public void SetReloadMultiplierParameter(AnimatorParameter value)
        {
            reloadMultiplierParameter = value;
        }

        public float GetReloadClipMultiplier()
        {
            return reloadClipMultiplier;
        }

        public void SetReloadClipMultiplier(float value)
        {
            reloadClipMultiplier = value;
            if (animator != null)
                animator.SetFloat(reloadClipMultiplierParameter, value);
        }

        public float GetReloadMultiplier()
        {
            return reloadMultiplier;
        }

        public void SetReloadMultiplier(float value)
        {
            reloadMultiplier = value;
            if (animator != null)
                animator.SetFloat(reloadMultiplierParameter, value);
        }
        #endregion
    }
}