﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules
{
    public partial class FootstepSoundSystem
    {
        public enum ProcessingType
        {
            AnimationEvent,
            Interval,
            Procedural
        }
    }
}