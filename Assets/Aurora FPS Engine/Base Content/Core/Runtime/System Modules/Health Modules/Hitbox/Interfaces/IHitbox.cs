﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules.HealthModules
{
    public interface IHitbox
    {
        HealthComponent GetTargetHealth();
    }
}