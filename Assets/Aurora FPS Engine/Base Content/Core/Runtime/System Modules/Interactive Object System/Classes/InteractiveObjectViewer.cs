﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Interactive Object/Interactive Object Viewer")]
    [DisallowMultipleComponent]
    public sealed class InteractiveObjectViewer : MonoBehaviour
    {
        [SerializeField]
        [NotNull]
        private Transform viewPoint;

        [SerializeField]
        [MinValue(0.01f)]
        private float range = 1.75f;

        [SerializeField]
        [Foldout("Advanced Settings", Style = "Header")]
        private LayerMask cullingLayer = 1 << 0;

        [SerializeField]
        [Foldout("Advanced Settings", Style = "Header")]
        private QueryTriggerInteraction triggerInteraction;

        // Stored required properties.
        private InteractiveObject activeObject;
        private bool isViewing = true;

        private void LateUpdate()
        {
            if (isViewing)
            {
                if (viewPoint != null &&
                    Physics.Raycast(viewPoint.position, viewPoint.forward, out RaycastHit hitInfo, range, cullingLayer, triggerInteraction))
                {
                    InteractiveObject interactiveObject = hitInfo.transform.GetComponent<InteractiveObject>();
                    if (interactiveObject != null && activeObject != interactiveObject)
                    {
                        activeObject?.Diactivate();
                        activeObject = interactiveObject;
                        activeObject.Activate(transform);
                    }
                }
                else if (activeObject != null)
                {
                    activeObject.Diactivate();
                    activeObject = null;
                }
            }
            else if (activeObject != null)
            {
                activeObject.Diactivate();
                activeObject = null;
            }
        }

        public void IsViewing(bool value)
        {
            isViewing = value;
        }
    }
}
