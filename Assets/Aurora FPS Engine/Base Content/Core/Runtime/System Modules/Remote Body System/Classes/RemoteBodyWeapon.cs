﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.CoreModules;
using AuroraFPSRuntime.CoreModules.Serialization.Collections;
using AuroraFPSRuntime.CoreModules.ValueTypes;
using AuroraFPSRuntime.SystemModules.InventoryModules;
using System.Collections.Generic;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/System Modules/Remote Body/Remote Body Weapon")]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Animator))]
    public sealed class RemoteBodyWeapon : MonoBehaviour
    {
        [System.Serializable]
        internal sealed class WeaponSettings
        {
            [SerializeField]
            [NotNull]
            private GameObject weapon;

            [SerializeField]
            private AnimatorState animatorState = "Reload";

            [SerializeField]
            private Transform leftHandTarget;

            [SerializeField]
            private Transform rightHandTarget;

            #region [Getter / Setter]
            public GameObject GetWeapon()
            {
                return weapon;
            }

            public void SetWeapon(GameObject value)
            {
                weapon = value;
            }

            public AnimatorState GetAnimatorState()
            {
                return animatorState;
            }

            public void SetAnimatorState(AnimatorState value)
            {
                animatorState = value;
            }

            public Transform GetLeftHandTarget()
            {
                return leftHandTarget;
            }

            public void SetLeftHandTarget(Transform value)
            {
                leftHandTarget = value;
            }

            public Transform GetRightHandTarget()
            {
                return rightHandTarget;
            }

            public void SetRightHandTarget(Transform value)
            {
                rightHandTarget = value;
            }
            #endregion
        }

        [System.Serializable]
        internal sealed class WeaponDictionary : SerializableDictionary<EquippableItem, WeaponSettings>
        {
            [SerializeField]
            private EquippableItem[] keys;

            [SerializeField]
            private WeaponSettings[] values;

            protected override EquippableItem[] GetKeys()
            {
                return keys;
            }

            protected override WeaponSettings[] GetValues()
            {
                return values;
            }

            protected override void SetKeys(EquippableItem[] keys)
            {
                this.keys = keys;
            }

            protected override void SetValues(WeaponSettings[] values)
            {
                this.values = values;
            }
        }

        [SerializeField]
        [NotNull]
        private InventorySystem inventorySystem;

        [SerializeField]
        private WeaponDictionary weaponDictionary;

        // Stored required components.
        private Animator animator;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            foreach (KeyValuePair<EquippableItem, WeaponSettings> item in weaponDictionary)
            {
                item.Value.GetWeapon().SetActive(false);
            }

            inventorySystem.OnEquipStartedCallback += (next) => 
            {
                foreach (KeyValuePair<EquippableItem, WeaponSettings> item in weaponDictionary)
                {
                    item.Value.GetWeapon().SetActive(false);
                }

                if (weaponDictionary.TryGetValue(next, out WeaponSettings settings))
                {
                    settings.GetWeapon().SetActive(true);
                    animator.CrossFadeInFixedTime(settings.GetAnimatorState());
                }
            };
        }

        private void OnAnimatorIK(int layerIndex)
        {
            Transform equippedTrasform = inventorySystem.GetEquippedTransform();
            EquippableItem equippableItem = inventorySystem.GetEquippedItem();
            if (equippedTrasform != null && equippableItem != null)
            {
                if (weaponDictionary.TryGetValue(equippableItem, out WeaponSettings settings))
                {

                    animator.SetIKPosition(AvatarIKGoal.LeftHand, settings.GetLeftHandTarget().position);
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1.0f);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, settings.GetLeftHandTarget().rotation);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1.0f);

                    animator.SetIKPosition(AvatarIKGoal.RightHand, settings.GetRightHandTarget().position);
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1.0f);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, settings.GetRightHandTarget().rotation);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1.0f);
                }
            }
        }
    }
}