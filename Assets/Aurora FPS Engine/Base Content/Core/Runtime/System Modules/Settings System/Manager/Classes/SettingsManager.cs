﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Aurora FPS Engine
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Shakirov, Alexandra Averyanova
   ---------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.CoreModules.Pattern;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules.SettingsSystem
{
    [HideScriptField]
   [AddComponentMenu(null)]
   [DisallowMultipleComponent]
   public abstract class SettingsManager : Singleton<SettingsManager>
   {
        public enum SettingsDirectory
        {
            PersistentDataDirectory,
            InstalledDataDirectory
        }

        [Serializable]
        public class SettingsSection
        {
            [SerializeField]
            private SettingsDirectory targetDirectory = SettingsDirectory.PersistentDataDirectory;

            [SerializeField]
            [NotEmpty]
            private string name = string.Empty;

            [SerializeField]
            [NotEmpty]
            private string fileName = string.Empty;

            [SerializeField]
            private string relativePath = "Data/Settings";

            [SerializeField]
            [NotEmpty]
            [Prefix("*.", Style = "Parameter")]
            private string fileExtension = "txt";

            #region [Getter / Setter]
            public SettingsDirectory GetTargetDirectory()
            {
                return targetDirectory;
            }

            public void SetTargetDirectory(SettingsDirectory value)
            {
                targetDirectory = value;
            }

            public string GetName()
            {
                return name;
            }

            public void SetName(string value)
            {
                name = value;
            }

            public string GetFileName()
            {
                return fileName;
            }

            public void SetFileName(string value)
            {
                fileName = value;
            }

            public string GetRelativePath()
            {
                return relativePath;
            }

            public void SetRelativePath(string value)
            {
                relativePath = value;
            }

            public string GetFileExtension()
            {
                return fileExtension;
            }

            public void SetFileExtension(string value)
            {
                fileExtension = value;
            }
            #endregion
        }

        [SerializeField]
        [ReorderableList(GetElementLabelCallback = "GetSettingsSectionLabel")]
        private SettingsSection[] settingsSections;

        // Stored required properties.
        private Dictionary<string, Dictionary<string, object>> buffer = new Dictionary<string, Dictionary<string, object>>();

        /// <summary>
        /// Called when the script instance is being loaded
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            LoadAll();
        }

        /// <summary>
        /// Save specified settings section.
        /// </summary>
        public void Save(string section)
        {
            if (ContainsSection(section))
            {
                OnBeforeSaveCallback?.Invoke(section);
                if(TryGetSettingsSectionPath(section, out string absolutePath))
                {
                    OnSave(absolutePath, buffer[section]);
                }
                OnSaveCallBack?.Invoke(section);
            }
        }

        /// <summary>
        /// Load specified settings section.
        /// </summary>
        public void Load(string section)
        {
            if (TryGetSettingsSectionPath(section, out string absolutePath))
            {
                OnLoad(absolutePath, out Dictionary<string, object> settingsBuffer);
                buffer[section] = settingsBuffer;
                OnLoadCallback?.Invoke(section);
            }
        }

        public void SaveAll()
        {
            for (int i = 0; i < settingsSections.Length; i++)
            {
                Save(settingsSections[i].GetName());
            }
        }

        public void LoadAll()
        {
            for (int i = 0; i < settingsSections.Length; i++)
            {
                Load(settingsSections[i].GetName());
            }
        }

        #region [Abstract methods]
        /// <summary>
        /// Save all current game settings to file
        /// </summary>
        /// <param name="settingsDataPath">Data file path</param>
        /// <param name="settings">Settings</param>
        public abstract void OnSave(string settingsDataPath, Dictionary<string, object> settings);

        /// <summary>
        /// Load all game settings from file
        /// </summary>
        /// <param name="path">Data file Path</param>
        /// <param name="settings">Settings</param>
        public abstract void OnLoad(string path, out Dictionary<string, object> settings);
        #endregion

        /// <summary>
        /// Absolute path to file, specified settings section name.
        /// </summary>
        public bool TryGetSettingsSectionPath(string sectionName, out string absolutePath)
        {
            if(TryGetSection(sectionName, out SettingsSection section))
            {
                string directory = null;
                switch (section.GetTargetDirectory())
                {
                    default:
                    case SettingsDirectory.PersistentDataDirectory:
                        directory = Application.persistentDataPath;
                        break;
                    case SettingsDirectory.InstalledDataDirectory:
                        directory = Application.dataPath;
                        break;
                }

                directory = Path.Combine(directory, section.GetRelativePath());
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string name = string.Format("{0}.{1}", section.GetFileName(), section.GetFileExtension());
                absolutePath = Path.Combine(directory, name);
                return true;
            }
            absolutePath = null;
            return false;
        }

        /// <summary>
        /// Add new settings item or replace value of the existing one
        /// </summary>
        /// <param name="section">Target settings section name.</param>
        /// <param name="guid">Item unique identifier.</param>
        /// <param name="value">Item value.</param>
        public void AddOrReplace(string section, string guid, object value)
        {
            buffer[section][guid] = value;
        }

        /// <summary>
        /// Remove settings item
        /// </summary>
        /// <param name="section">Target settings section name.</param>
        /// <param name="guid">Item unique identifier.</param>
        /// <returns>If item with specific id existed in dictionary and was removed.</returns>
        public bool RemoveItem(string section, string guid)
        {
            if(buffer.TryGetValue(section, out Dictionary<string, object> sectionBuffer))
            {
                return sectionBuffer.Remove(guid);
            }
            return false;
        }

        /// <summary>
        /// Determines whether there is a section.
        /// </summary>
        /// <param name="section">Settings section name.</param>
        public bool ContainsSection(string section)
        {
            return buffer.ContainsKey(section);
        }

        /// <summary>
        /// Determines whether there is a key in the section.
        /// </summary>
        /// <param name="section">Settings section name.</param>
        /// <param name="guid">Item unique identifier.</param>
        public bool ContainsItem(string section, string guid)
        {
            if (buffer.TryGetValue(section, out Dictionary<string, object> sectionBuffer))
            {
                return sectionBuffer.ContainsKey(guid);
            }
            return false;
        }

        /// <summary>
        /// Try to get settings item with specific id
        /// </summary>
        /// <param name="guid">Item unique identifier</param>
        /// <param name="item">Returning item</param>
        /// <param name="removeItem">Remove item after getting</param>
        /// <returns>If item with specific id exists in dictionary</returns>
        public bool TryGetItem(string section, string guid, out object item, bool removeItem = true)
        {
            if (buffer.TryGetValue(section, out Dictionary<string, object> sectionBuffer))
            {
                if (sectionBuffer.TryGetValue(guid, out item))
                {
                    if (removeItem)
                    {
                        buffer.Remove(guid);
                    }
                    return true;
                }
            }
            item = null;
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool TryGetSection(string name, out SettingsSection section)
        {
            for (int i = 0; i < settingsSections.Length; i++)
            {
                section = settingsSections[i];
                if(section.GetName() == name)
                {
                    return true;
                }
            }
            section = null;
            return false;
        }

        #region[Event Callback Functions]
        /// <summary>
        /// Called when specified section being loaded.
        /// </summary>
        public event Action<string> OnLoadCallback;

        /// <summary>
        /// Called when specified section starts to be saved.
        /// </summary>
        public event Action<string> OnBeforeSaveCallback;

        /// <summary>
        /// Called when specified section being saved.
        /// </summary>
        public event Action<string> OnSaveCallBack;
        #endregion

        #region [Editor Section]
#if UNITY_EDITOR
        private string GetSettingsSectionLabel(UnityEditor.SerializedProperty property, int index)
        {
            string label = property.FindPropertyRelative("name").stringValue;
            return !string.IsNullOrEmpty(label) ? label : string.Format("Section {0}", index + 1);
        }

#endif
        #endregion

        #region [Getter / Setter]
        public SettingsSection[] GetSettingsSections()
        {
            return settingsSections;
        }

        public void SetSettingsSections(SettingsSection[] value)
        {
            settingsSections = value;
        }
        #endregion
    }
}

