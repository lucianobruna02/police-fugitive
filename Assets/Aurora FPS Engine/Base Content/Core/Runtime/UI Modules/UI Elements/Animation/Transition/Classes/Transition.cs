﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.CoreModules.Coroutines;
using AuroraFPSRuntime.Attributes;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace AuroraFPSRuntime.UIModules.UIElements.Animation
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/UI Modules/UI Elements/Animation/Transition")]
    [DisallowMultipleComponent]
    public abstract class Transition : MonoBehaviour
    {
        public enum AwakeAction
        {
            Nothing,
            FadeIn,
            FadeOut
        }

        [SerializeField]
        [MinValue(0.01f)]
        [Order(501)]
        private float duration = 0.25f;

        [SerializeField]
        [Indent(1)]
        [Order(502)]
        private AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [SerializeField]
        [Foldout("Event Callbacks", Style = "Header")]
        [Order(601)]
        private UnityEvent onFadeInCompleteEvent;

        [SerializeField]
        [Foldout("Event Callbacks", Style = "Header")]
        [Order(602)]
        private UnityEvent onFadeOutCompleteEvent;

        [SerializeField]
        [Label("On Awake Action")]
        [Foldout("Advanced Settings", Style = "Header")]
        [Order(701)]
        private AwakeAction awakeAction = AwakeAction.Nothing;

        // Stored required properties.
        private CoroutineObject coroutineObject;

        /// <summary>
        /// Сalled when the script instance is being loaded.
        /// </summary>
        protected virtual void Awake()
        {
            coroutineObject = new CoroutineObject(this);
            switch (awakeAction)
            {
                case AwakeAction.FadeIn:
                    FadeIn();
                    break;
                case AwakeAction.FadeOut:
                    FadeOut();
                    break;
            }

            OnFadeInCompleteCallback += onFadeInCompleteEvent.Invoke;
            OnFadeOutCompleteCallback += onFadeOutCompleteEvent.Invoke;
        }

        #region [Abstract Methods]
        /// <summary>
        /// Implement this method to make fade in logic.
        /// </summary>
        /// <param name="smooth">Smooth interpolation value.</param>
        protected abstract void OnFadeIn(float smooth);

        /// <summary>
        /// Implement this method to make fade out logic.
        /// </summary>
        /// <param name="smooth">Smooth interpolation value.</param>
        protected abstract void OnFadeOut(float smooth);
        #endregion

        /// <summary>
        /// Make transition to fade in.
        /// </summary>
        public void FadeIn()
        {
            coroutineObject.Start(WaitForFadeIn, true);
        }

        /// <summary>
        /// Make transition to fade out.
        /// </summary>
        public void FadeOut()
        {
            coroutineObject.Start(WaitForFadeOut, true);
        }

        /// <summary>
        /// Transition processing.
        /// </summary>
        public IEnumerator WaitForFadeIn()
        {
            float speed = 1 / duration;
            float time = 0.0f;

            while (time < 1.0f)
            {
                time += Time.deltaTime * speed;
                float smooth = curve.Evaluate(time);
                OnFadeIn(smooth);
                yield return null;
            }
            OnFadeInCompleteCallback?.Invoke();
        }

        /// <summary>
        /// Transition processing.
        /// </summary>
        public IEnumerator WaitForFadeOut()
        {
            float speed = 1 / duration;
            float time = 0.0f;

            while (time < 1.0f)
            {
                time += Time.deltaTime * speed;
                float smooth = curve.Evaluate(time);
                OnFadeOut(smooth);
                yield return null;
            }
            OnFadeOutCompleteCallback?.Invoke();
        }

        #region [Event Callback Functions]
        public event System.Action OnFadeInCompleteCallback;

        public event System.Action OnFadeOutCompleteCallback;
        #endregion
    }
}
