﻿/* ================================================================
   ---------------------------------------------------
   Project   :    Aurora FPS Engine
   Publisher :    Infinite Dawn
   Author    :    Tamerlan Shakirov
   ---------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using UnityEngine;

namespace AuroraFPSRuntime.SystemModules.SettingsSystem
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/UI Modules/UI Elements/Buttons/Save Setting Handler")]
    [DisallowMultipleComponent]
    public sealed class SaveSettingsButtonHandler : MonoBehaviour
    {
        // Stored required components.
        private SettingsManager settingsManager;

        /// <summary>
        /// Called when the script instance is being loaded
        /// </summary>
        private void Awake()
        {
            settingsManager = SettingsManager.GetRuntimeInstance();
        }

        /// <summary>
        /// Save specified section of settings.
        /// </summary>
        public void SaveSection(string section)
        {
            settingsManager.Save(section);
        }

        /// <summary>
        /// Save all sections of settings.
        /// </summary>
        public void SaveAll(string section)
        {
            settingsManager.SaveAll();
        }
    }
}
