﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.SystemModules.HealthModules;
using UnityEngine.UI;
using UnityEngine;

namespace AuroraFPSRuntime.UIModules.UIElements.HUD
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/UI Modules/UI Elements/HUD/Health/Damage Screen Drawer")]
    public sealed class DamageScreenDrawer : MonoBehaviour
    {
        [SerializeField]
        private int startDrawPoint = 25;

        [SerializeField]
        [NotNull]
        private CharacterHealth characterHealth;

        // Stored required components;
        private Image image;
        
        /// <summary>
        /// Сalled when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            image = GetComponent<Image>();
        }

        /// <summary>
        /// Called every frame, while the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            Color color = image.color;
            color.a = Mathf.InverseLerp(startDrawPoint, characterHealth.GetMinHealth(), characterHealth.GetHealth());
            image.color = color;
        }
    }
}
