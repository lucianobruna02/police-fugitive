﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.UIModules.UIElements.Animation;
using AuroraFPSRuntime.SystemModules.ControllerModules;
using UnityEngine;

namespace AuroraFPSRuntime.WeaponModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/Weapon Modules/Sight/Holographic Sight")]
    [System.Obsolete]
    public sealed class HolographicSight : MonoBehaviour
    {
        [SerializeField]
        private Transition transition;

        // Stored required components.
        private Controller controller;

        private void Awake()
        {
            controller = transition.GetComponentInParent<Controller>();
        }

        private void OnEnable()
        {
            if (controller != null)
            {
                FPCameraControl cameraControl = controller.GetCameraControl();
                cameraControl.OnStartZoomCallback += transition.FadeIn;
                cameraControl.OnStopZoomCallback += transition.FadeOut;
            }
        }

        private void OnDisable()
        {
            if (controller != null)
            {
                FPCameraControl cameraControl = controller.GetCameraControl();
                cameraControl.OnStartZoomCallback -= transition.FadeIn;
                cameraControl.OnStopZoomCallback -= transition.FadeOut;
                transition.FadeOut();
            }
        }
    }
}