﻿/* ================================================================
   ----------------------------------------------------------------
   Project   :   Aurora FPS Engine
   Publisher :   Infinite Dawn
   Developer :   Tamerlan Shakirov
   ----------------------------------------------------------------
   Copyright © 2017 Tamerlan Shakirov All rights reserved.
   ================================================================ */

using AuroraFPSRuntime.Attributes;
using AuroraFPSRuntime.SystemModules.ControllerModules;
using UnityEngine;

namespace AuroraFPSRuntime.WeaponModules
{
    [HideScriptField]
    [AddComponentMenu("Aurora FPS Engine/Weapon Modules/Sight/Sniper Sight")]
    [DisallowMultipleComponent]
    public sealed class SniperSight : MonoBehaviour
    {
        [SerializeField]
        [ReorderableList(ElementLabel = null)]
        private MeshRenderer[] sightComponents;

        [SerializeField]
        [ReorderableList(ElementLabel = null)]
        private MeshRenderer[] plugComponents;

        // Stored required properties.
        private Controller controller;

        private void Awake()
        {
            controller = GetComponentInParent<Controller>();
            if(controller != null)
            {
                controller.GetCameraControl().OnFOVProgressCallback += OnFOVChanged;
            }
        }

        private void OnDestroy()
        {
            if (controller != null)
            {
                controller.GetCameraControl().OnFOVProgressCallback -= OnFOVChanged;
            }
        }

        private void OnFOVChanged(float progress)
        {
            FPCameraControl cameraControl = controller.GetCameraControl();
            for (int i = 0; i < sightComponents.Length; i++)
            {
                MeshRenderer meshRenderer = sightComponents[i];
                if(meshRenderer != null)
                {
                    Material material = meshRenderer.material;
                    Color targetColor = material.color;
                    targetColor.a = cameraControl.IsZooming() ? 1.0f : 0.0f;
                    material.color = Color.Lerp(material.color, targetColor, progress);
                }
            }

            for (int i = 0; i < plugComponents.Length; i++)
            {
                MeshRenderer meshRenderer = plugComponents[i];
                if (meshRenderer != null)
                {
                    Material material = meshRenderer.material;
                    Color targetColor = material.color;
                    targetColor.a = cameraControl.IsZooming() ? 0.0f : 1.0f;
                    material.color = Color.Lerp(material.color, targetColor, progress);
                }
            }
        }
    }
}